//
//  GeneratedGraph.swift
//  StarApp
//
//  Created by savemywater on 24/05/2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

@IBDesignable
class GeneratedGraph: UIView {
    
    // Variable
    var nbRect: Int = 0
    var maxHeight: Int = 100
    var maxWidth: Int = 100
    var borderAll: [([Int: Int], Int)] = []
    
    // Constructeur
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup() {
        drawGraph()
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! GeneratedGraph
        
        return view
    }
    
    // Inspectable pour storyboard
    @IBInspectable var NbRect: Int {
        get {
            return nbRect
        }
        set {
            nbRect = newValue
        }
    }
    
    @IBInspectable var MaxHeight: Int {
        get {
            return maxHeight
        }
        set {
            maxHeight = newValue
        }
    }
    
    @IBInspectable var MaxWidth: Int {
        get {
            return maxWidth
        }
        set {
            maxWidth = newValue
        }
    }
    
    // Pour Builder
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    // Fonction
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        drawGraph()
    }
    
    func drawGraph() {
        var height: Int = 0
        var width: Int = 0
        var x: Int  = 0
        var y: Int = 0
        var rect: CGRect
        var bPath: UIBezierPath
        let color = UIColor.blue.withAlphaComponent(0.3)
        
        for _ in 0..<nbRect {
            height = Int(arc4random_uniform(UInt32(maxHeight)))
            width = Int(arc4random_uniform(UInt32(maxWidth)))
            
            y = Int(frame.size.height) - height - 1
            
            rect = CGRect(x: x%500, y: y, width: width, height: height)
            bPath = UIBezierPath(rect: rect)
            
            color.setFill()
            bPath.fill()
    
            borderAll.append(([x%500 : x%500+width], height))
            
            x += width
        }
        
        drawBorder()
    }
    
    func drawBorder() {
        let stroke = UIColor.black
        let dict = Array(getBorder()).sorted(by: { $0.0 < $1.0 })
        var height: Int = 0
        var width: Int = 0
        var x: Int = 0
        var y: Int = 0
        var rect: CGRect
        var bPath: UIBezierPath
        
        for i in 0..<dict.count-1 {
            // On ajoute la bordure horizontal
            y = Int(frame.size.height) - dict[i].value - 1
            width = dict[i+1].key - dict[i].key
            rect = CGRect(x: x, y: y, width: width, height: 1)
            bPath = UIBezierPath(rect: rect)
            
            stroke.setFill()
            bPath.fill()
            
            x += width
            
            // On ajoute la bordure vertical
            height = dict[i].value - dict[i+1].value
            rect = CGRect(x: x-1, y: y, width: 1, height: height)
            bPath = UIBezierPath(rect: rect)
            
            stroke.setFill()
            bPath.fill()
        }
    }
    
    func getBorder() -> [Int: Int] {
        var border: [Int: Int] = [0: 0, 500: 0]
        var max: Int = 0
        var last: Int = -1
        
        for i in 0...Int(frame.size.width) {
            // On recupere la plus grande valeur pour x
            max = getMax(x: i)
            // Si la valeur differe de la valeur précédente on ajoute au dictionnaire
            if max != last {
                border.updateValue(max, forKey: i)
                last = max
            }
        }
        
        return border
    }
    
    func getMax(x: Int) -> Int {
        var max: Int = 0
        
        for item in borderAll {
            if x >= Array(item.0)[0].key && x <= Array(item.0)[0].value {
                if item.1 > max {
                    max = item.1
                }
            }
        }
        
        return max
    }

}
