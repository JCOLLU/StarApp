//
//  StarView.swift
//  StarApp
//
//  Created by Collu Johan on 18/05/2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

@IBDesignable
class StarView: UIView {
    
    // Variable
    var iRadius: CGFloat = 40.0
    var spikes: Int = 6
    var width: CGFloat = 3.0
    var stroke: UIColor?
    var fill: UIColor?
    
    // Constructeur
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        drawStar(sides: spikes)
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! StarView
        
        return view
    }
    
    // Inspectable pour storyboard
    @IBInspectable var InnerRadius: CGFloat {
        get {
            return iRadius
        }
        set {
            iRadius = newValue
        }
    }
    
    @IBInspectable var Spikes: Int {
        get {
            return spikes
        }
        set {
            spikes = newValue
        }
    }
    
    @IBInspectable var LineWidth: CGFloat {
        get {
            return width
        }
        set {
            width = newValue
        }
    }
    
    @IBInspectable var StrokeColor: UIColor? {
        get {
            return stroke!
        }
        set {
            stroke = newValue
        }
    }
    
    @IBInspectable var fillColor: UIColor? {
        get {
            return fill!
        }
        set {
            fill = newValue
        }
    }
    
    // Pour Builder
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    // Fonction
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        drawStar(sides: spikes)
    }
    
    func degree2radian(a:CGFloat) -> CGFloat {
        let b = CGFloat(Double.pi) * a/180
        return b
    }
    
    func polygonPointArray(sides:Int,x:CGFloat,y:CGFloat,radius:CGFloat,adjustment:CGFloat=0) -> [CGPoint] {
        let angle = degree2radian(a: 360/CGFloat(sides))
        let cx = x
        let cy = y
        let r  = radius
        var i = sides
        var points = [CGPoint]()
        while points.count <= sides {
            let xpo = cx - r * cos(angle * CGFloat(i)+degree2radian(a: adjustment))
            let ypo = cy - r * sin(angle * CGFloat(i)+degree2radian(a: adjustment))
            points.append(CGPoint(x: xpo, y: ypo))
            i -= 1;
        }
        return points
    }
    
    func starPath(x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, iRadius:CGFloat, startAngle:CGFloat=0) -> CGPath {
        let adjustment = startAngle + CGFloat(360/sides/2)
        let path = CGMutablePath.init()
        let ratio = iRadius / 100
        let points = polygonPointArray(sides: sides,x: x,y: y,radius: radius*ratio, adjustment: startAngle)
        let cpg = points[0]
        let points2 = polygonPointArray(sides: sides,x: x,y: y,radius: radius,adjustment:CGFloat(adjustment))
        var i = 0
        path.move(to: CGPoint(x:cpg.x,y:cpg.y))
        for p in points {
            path.addLine(to: CGPoint(x:points2[i].x, y:points2[i].y))
            path.addLine(to: CGPoint(x:p.x, y:p.y))
            i += 1
        }
        path.closeSubpath()
        return path
    }
    
    func drawStarBezier(x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, iRadius:CGFloat) -> UIBezierPath {
        var startAngle: CGFloat = 0.0
        
        switch sides%4 {
            case 0:
                startAngle = CGFloat(360/sides/2)
                break
            case 1:
                startAngle = CGFloat(-360/sides/4)
                break
            case 2:
                startAngle = CGFloat(360/sides)
                break
            case 3:
                startAngle = CGFloat(360/sides/4)
                break
            default:
                startAngle = CGFloat(360/sides)
        }
        
        let path = starPath(x: x, y: y, radius: radius, sides: sides, iRadius: iRadius, startAngle: startAngle)
        let bez = UIBezierPath(cgPath: path)
        return bez
    }
    
    func drawStar(sides:Int) {
        let radius = frame.size.width/3
        let penStar:UIBezierPath = drawStarBezier(x: frame.size.width/2, y: frame.size.height/2, radius: radius, sides: sides, iRadius:iRadius)
        
        fill?.setFill()
        stroke?.setStroke()
        penStar.lineWidth = width
        penStar.stroke()
        penStar.fill()
    }
}
